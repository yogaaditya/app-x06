package aditya.yoga.app06_2e

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragKlx : FragmentPerkalian
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragKlx = FragmentPerkalian()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProdi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMhs).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> frameLayout.visibility = View.GONE
            R.id.itemPolinema ->{
                var webUri = "http://polinema.ac.id"
                var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                startActivity(intentInternet)
            }
            R.id.itemPerkalian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragKlx).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }

        }
        return true
    }
}