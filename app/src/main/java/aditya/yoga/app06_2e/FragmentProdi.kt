package aditya.yoga.app06_2e

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class FragmentProdi : Fragment() {

    lateinit var thisParent : MainActivity
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity

        return inflater.inflate(R.layout.frag_data_prodi,container,false)
    }
}